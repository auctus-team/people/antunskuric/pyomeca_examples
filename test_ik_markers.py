import numpy as np
import biorbd

try:
    import bioviz

    biorbd_viz_found = True
except ModuleNotFoundError:
    biorbd_viz_found = False

# Load a predefined model
model = biorbd.Model("pyomeca_models/MOBL_ARMS_fixed_33.bioMod")
# model = biorbd.Model("pyomeca_models/BrasCompletKinect.bioMod")
nq = model.nbQ()
nb_mus = model.nbMuscles()


# Generate clapping gesture data
n_frames = 10
qinit = np.array([0, 0.3, -0.3, 0.35, 1.15, -0.0, 0])
qmid = np.array([0.0, 0, -0.3, 1.5, 1.8, 0.2, 0])
qfinal = np.array([0, 0.3, -0.3, 0.35, 1.15, -0.3, 0 ])
target_q = np.concatenate((np.linspace(qinit, qmid, n_frames).T, np.linspace(qmid, qfinal, n_frames).T), axis=1)
markers = np.ndarray((3, model.nbMarkers(), 2 * n_frames))


## generate the marker data 
for i, q in enumerate(target_q.T):
    markers[:, :, i] = np.array([mark.to_array() for mark in model.markers(q)]).T
# Dispatch markers in biorbd structure so EKF can use it
markersOverFrames = []
for i in range(markers.shape[2]):
    markersOverFrames.append([biorbd.NodeSegment(m) for m in markers[:, :, i].T])

# Create a Kalman filter structure
freq = 100  # Hz
params = biorbd.KalmanParam(freq)
kalman = biorbd.KalmanReconsMarkers(model, params)

# Perform the kalman filter for each frame (the first frame is much longer than the next)
Q = biorbd.GeneralizedCoordinates(qinit)
Qdot = biorbd.GeneralizedVelocity(model)
Qddot = biorbd.GeneralizedAcceleration(model)
q_recons = np.ndarray((model.nbQ(), len(markersOverFrames)))
for i, targetMarkers in enumerate(markersOverFrames):
    kalman.reconstructFrame(model, targetMarkers, Q, Qdot, Qddot)
    q_recons[:, i] = Q.to_array()

    # Print the kinematics to the console
    print(f"Frame {i}\nExpected Q = {target_q[:, i]}\nCurrent Q = {q_recons[:, i]}\n")

# Animate the results if biorbd viz is installed
if biorbd_viz_found:
    b = bioviz.Viz(loaded_model=model)
    b.load_movement(q_recons)
    b.exec()
