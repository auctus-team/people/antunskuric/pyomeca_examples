import numpy as np
import biorbd


# Load a predefined model
model = biorbd.Model("pyomeca_models/MOBL_ARMS_fixed_33.bioMod")
nq = model.nbQ()
nqdot = model.nbQdot()
nqddot = model.nbQddot()

# Choose a position/velocity/acceleration to compute dynamics from
Q = np.zeros((nq,))
Qdot = np.zeros((nqdot,))
Qddot = np.zeros((nqddot,))

# Proceed with the inverse dynamics
Tau_grav = model.InverseDynamics(Q, Qdot, Qddot)

# Print them to the console
print(Tau_grav.to_array())
