import numpy as np
import biorbd

try:
    import bioviz

    biorbd_viz_found = True
except ModuleNotFoundError:
    biorbd_viz_found = False

# Load a predefined model
model = biorbd.Model("pyomeca_models/BrasCompletIMUS2.bioMod")
nq = model.nbQ()
nb_mus = model.nbMuscles()

# Generate clapping gesture data
n_frames = 20
qinit = np.array([ 0, -0.3, 0.35, 1.15, -0.35, 1.15, 0])
qmid = np.array([-1, -0.3, 1.5, 0.3, -0.5, 1.15, 0.5])
qfinal = np.array([ 0, -0.3, 0.35, 1.15, -0.35, 1.15, 0 ])
target_q = np.concatenate((np.linspace(qinit, qmid, n_frames).T, np.linspace(qmid, qfinal, n_frames).T), axis=1)


## generate the IMU data
imusOverFrames = np.ndarray((2 * n_frames, 9* model.nbIMUs()))
for frame, q in enumerate(target_q.T):
    imusOverFrames[frame, :] = np.hstack([imu.to_array()[:3,:3].T.flatten() for imu in model.IMU(q)])

# Create a Kalman filter structure
freq = 100  # Hz
params = biorbd.KalmanParam(freq)
# kalman = biorbd.KalmanReconsMarkers(model, params)
kalman = biorbd.KalmanReconsIMU(model, params)


# Perform the kalman filter for each frame (the first frame is much longer than the next)
Q = biorbd.GeneralizedCoordinates(qinit)
Qdot = biorbd.GeneralizedVelocity(model)
Qddot = biorbd.GeneralizedAcceleration(model)
q_recons = np.ndarray((model.nbQ(), len(imusOverFrames)))
for i, targetIMUs in enumerate(imusOverFrames):
    kalman.reconstructFrame(model, targetIMUs, Q, Qdot, Qddot)
    q_recons[:, i] = Q.to_array()

    # Print the kinematics to the console
    #print(f"Frame {i}\nExpected Q = {target_q[:, i]}\nCurrent Q = {q_recons[:, i]}\n")

# Animate the results if biorbd viz is installed
if biorbd_viz_found:
    b = bioviz.Viz(loaded_model=model)
    b.load_movement(q_recons)
    b.exec()
